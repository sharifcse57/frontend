import http from "../http-common";

class RegistrationDataService {

    create(data) {
        return http.post("/register", data);
    }

}

export default new RegistrationDataService();