import http from "../http-login";

class LoginDataService {

    attempt(data) {
        return http.post("/oauth/token", data);
    }

}

export default new LoginDataService();